package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;
import java.util.List;

public class CardGame extends BoardGame {
    protected List<Card> cards;
    protected Integer numberOfRounds = 1;


    public CardGame(String name, Integer maxPlayers, String filename) {
        super(name, maxPlayers, filename);
        this.board = new CardBoard();
    }

    public void initialize(String filename) {
        try {
            File cardFile = new File(filename);
            Scanner myReader = new Scanner(cardFile);
            this.cards = new ArrayList<>();
            while (myReader.hasNextLine()) {
                String[] data = myReader.nextLine().split(";");
                this.cards.add(new Card(data[0], Integer.parseInt(data[1]), true));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred .");
            e.printStackTrace();
        }

    }

    public Player run(Map<Player, Socket> joueursConnecte) throws IOException, ClassNotFoundException {
        //Map permettant de sauvegarder qui a jouer quelle carte
        Map<Player, Card> playedCard = new HashMap<>();
        //Variable joueur permettant de désigner le vainqueur de chaque round
        Player Vainqueur = null;


        //Compteur pour répartir les cartes entre joueurs.
        int compteur = 0;

        //mélange des cartes
        Collections.shuffle(cards);

        //distribution des cartes entre les joueurs
        for (Card carte : cards) {
            carte.setHidden(false);
            players.get(compteur % players.size()).addComponent(carte);
            compteur++;
        }

        //Chaque joueur est prêt à jouer désormais
        for (Player joueur : players) {
            if (joueur.getScore() > 0) {
                joueur.canPlay(true);
            }
        }


        //Affichage des joueurs présents et leur nombre de cartes à chaque joueur.
        for (Map.Entry<Player, Socket> entry : joueursConnecte.entrySet()) {
            Socket socket = entry.getValue();
            StringBuilder statut = new StringBuilder();
            for (Player joueur : players) {
                if (joueur.isPlaying()) {
                    statut.append(joueur.getName()).append(" : ").append(joueur.getScore()).append("\n");
                }
            }
            statut.append("------------------------\n");
            ObjectOutputStream toutEstOkOos = new ObjectOutputStream(socket.getOutputStream());
            toutEstOkOos.writeObject(statut);
        }

        //Tant que le jeu n'est pas fini (qu'il n'y a pas de vainqueur quoi)
        while (!end()) {
            Integer maxValue = 0;
            //on fait jouer chaque joueur qui peux jouer
            for (Map.Entry<Player, Socket> entry : joueursConnecte.entrySet()) {
                Player joueur = players.get(players.indexOf(entry.getKey()));
                Socket socket = entry.getValue();
                if (joueur.isPlaying()) {

                    //Envois des données du jeu
                    ObjectOutputStream gameOos = new ObjectOutputStream(socket.getOutputStream());
                    gameOos.writeObject(this);
                    //Affichage aux clients de qui doit jouer...
                    for (Map.Entry<Player, Socket> waiting : joueursConnecte.entrySet()) {
                        Player cible = waiting.getKey();
                        Socket socket1 = waiting.getValue();
                        String waitForAction = "";
                        if (cible != joueur) {
                            waitForAction += "Waiting for " + joueur.getName() + " to play...";
                        } else {
                            waitForAction += "[" + cible.getName() + "] you have to play";
                        }
                        ObjectOutputStream waitForActionOos = new ObjectOutputStream(socket1.getOutputStream());
                        waitForActionOos.writeObject(waitForAction);
                    }
                    //le joueur joue sa carte
                    //Réception de l'action
                    ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                    Card card = (Card) ois.readObject();
                    //On cherche la carte à supprimer
                    for (Component carte : joueur.getSpecificComponents(Card.class)) {
                        if (card.getValue().equals(carte.getValue())) {
                            card = (Card) carte;
                            break;
                        }
                    }
                    //On la supprime
                    joueur.removeComponent(card);
                    //Si le joueur a joué une meilleure carte, il est déclaré vainqueur du round jusqu'a ce qu'un autre joueur mette une meilleure carte.
                    if (card.getValue() > maxValue) {
                        maxValue = card.getValue();
                        Vainqueur = joueur;
                        //Si la valeur est identique, le vainqueur est tiré au sort via une comparaison sur un chiffre aléatoire.
                    } else if (card.getValue().equals(maxValue) && Math.random() > 0.5) {
                        maxValue = card.getValue();
                        Vainqueur = joueur;
                    }
                    //On ajoute la carte et le joueur associé dans la liste.
                    playedCard.put(joueur, card);
                    board.addComponent(card);

                    //On affiche à chaque joueur la carte que vient de jouer le joueur jouant.
                    for (Map.Entry<Player, Socket> waiting : joueursConnecte.entrySet()) {
                        Socket socket1 = waiting.getValue();
                        ObjectOutputStream playedCardOos = new ObjectOutputStream(socket1.getOutputStream());
                        playedCardOos.writeObject(joueur.getName() + " has played " + card.getName());
                    }
                }
            }

            //Affichage plateau, puis du vainqueur et actualisation du jeu
            for (Map.Entry<Player, Socket> waiting : joueursConnecte.entrySet()) {
                Socket socket1 = waiting.getValue();
                String separateur = "\n------------------------\n";
                ObjectOutputStream gameOos = new ObjectOutputStream(socket1.getOutputStream());
                gameOos.writeObject(this);
                ObjectOutputStream boardOos = new ObjectOutputStream(socket1.getOutputStream());
                boardOos.writeObject(board);
                ObjectOutputStream separateurOos = new ObjectOutputStream(socket1.getOutputStream());
                separateurOos.writeObject(separateur);
                ObjectOutputStream vainqueurOos = new ObjectOutputStream(socket1.getOutputStream());
                vainqueurOos.writeObject(Vainqueur.getName() + " win this round !\n");
            }

            //le vainqueur raffle la mise, on vérifie aussi les joueurs qui ne peuvent plus jouer au passage
            for (Map.Entry<Player, Card> entry : playedCard.entrySet()) {
                Card carte = entry.getValue();
                Player ancienProprio = entry.getKey();
                Vainqueur.addComponent(carte);
                //Si il y a un perdant on notifie tout le monde et on l'empêche de continuer à jouer.
                if (ancienProprio.getScore() == 0) {
                    ancienProprio.canPlay(false);
                    for (Map.Entry<Player, Socket> waiting : joueursConnecte.entrySet()) {
                        Player cible = waiting.getKey();
                        Socket socket = waiting.getValue();
                        ObjectOutputStream loserOos = new ObjectOutputStream(socket.getOutputStream());
                        if (ancienProprio != cible) {
                            loserOos.writeObject(ancienProprio.getName() + " lost !!");
                        } else {
                            loserOos.writeObject("You have lost !");
                        }
                    }
                }
            }
            //on oublie pas de nettoyer les cartes jouées.
            playedCard.clear();
            board.clear();

            //affichage des scores
            for (Map.Entry<Player, Socket> waiting : joueursConnecte.entrySet()) {
                Socket socket = waiting.getValue();
                ObjectOutputStream scoreOos = new ObjectOutputStream(socket.getOutputStream());
                StringBuilder score = new StringBuilder();
                for (Player joueur : players) {
                    if (joueur.isPlaying()) {
                        score.append(joueur.getName()).append(" : ").append(joueur.getScore()).append("\n");
                    }
                }
                scoreOos.writeObject("Round: " + numberOfRounds +
                        " ; Score: \n" + score.toString() +
                        "\n------------------------\n");
            }


            //Si on atteint un modulo 10 round, tout les joueurs mélangent leurs mains.
            if (this.numberOfRounds % 10 == 0) {
                for (Map.Entry<Player, Socket> waiting : joueursConnecte.entrySet()) {
                    Socket socket = waiting.getValue();
                    ObjectOutputStream shuffleOos = new ObjectOutputStream(socket.getOutputStream());
                    shuffleOos.writeObject("Shuffle...\n");
                }
                for (Player joueur : players) {
                    joueur.shuffleHand();
                }
            }

            //à la fin de chaque round on oublie pas d'incrémenter le compteur de rounds...
            numberOfRounds++;
        }

        return Vainqueur;
    }

    public boolean end() {
        for (Player joueur : players) {
            if (joueur.getScore() == cards.size()) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return this.name;
    }

}

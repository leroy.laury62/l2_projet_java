package ulco.cardGame.common.games;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.*;

public class BattleGame extends CardGame {

    public BattleGame(String name, Integer maxPlayers, String filename) {
        super(name, maxPlayers, filename);
    }

    public Player run(Map<Player, Socket> joueursConnecte) throws IOException, ClassNotFoundException {
        //Map permettant de sauvegarder qui a jouer quelle carte
        Map<Card, Player> playedCard = new HashMap<>();


        //Map permettant de sauvegarder les joueurs qui ont joués une carte qui ne leurs appartenaient pas durant une bataille (utile si plusieurs batailles s'enchainent)
        Map<Card, Player> playedCardNotPossessed = new HashMap<>();

        //Hashset permettant de connaitre les joueurs participant à la bataille
        HashMap<Player, Socket> playerInBataille = new HashMap<>();

        //Permettant de savoir les joueurs ayant besoin de carte pour une bataille
        HashMap<Player, Socket> playerInNeed = new HashMap<>();

        //Permettant de savoir les joueurs pouvant aider;
        HashMap<Player, Socket> playerCanHelp = new HashMap<>();

        //Variable joueur permettant de désigner le vainqueur de chaque round
        Player Vainqueur = null;

        //scanner pour éviter que le jeu enchaine jusqu'à la fin sans attendre...
        Scanner scanner = new Scanner(System.in);

        //Compteur pour répartir les cartes entre joueurs.
        int compteur = 0;

        //Boolean qui vérifie si on doit jouer une bataille, se réinitialise à chaque tour.
        boolean bataille = false;

        //mélange des cartes
        Collections.shuffle(cards);

        //distribution des cartes entre les joueurs
        for (Card carte : cards) {
            carte.setHidden(false);
            players.get(compteur % players.size()).addComponent(carte);
            compteur++;
        }

        //Chaque joueur est prêt à jouer désormais
        for (Player joueur : players) {
            if (joueur.getScore() > 0) {
                joueur.canPlay(true);
            }
        }

        //Affichage des joueurs présents et leur nombre de cartes à chaque joueur.
        StringBuilder statut = new StringBuilder();
        for (Player joueur : players) {
            if (joueur.isPlaying()) {
                statut.append(joueur.getName()).append(" : ").append(joueur.getScore()).append("\n");
            }
        }
        statut.append("------------------------\n");
        messageToAllClient(joueursConnecte, statut);

        //Tant que le jeu n'est pas fini (qu'il n'y a pas de vainquer quoi)
        while (!end()) {
            Integer maxValue = 0;
            //on fait jouer chaque joueur qui peux jouer
            for (Map.Entry<Player, Socket> entry : joueursConnecte.entrySet()) {
                Player joueur = players.get(players.indexOf(entry.getKey()));
                Socket socket = entry.getValue();
                if (joueur.isPlaying()) {

                    //Envois des données du jeu
                    messageToClient(socket, this);
                    //Affichage aux clients de qui doit jouer...
                    for (Map.Entry<Player, Socket> waiting : joueursConnecte.entrySet()) {
                        Player cible = waiting.getKey();
                        Socket socket1 = waiting.getValue();
                        String waitForAction = "";
                        if (cible != joueur) {
                            waitForAction += "Waiting for " + joueur.getName() + " to play...";
                        } else {
                            waitForAction += "[" + cible.getName() + "] you have to play";
                        }
                        messageToClient(socket1, waitForAction);
                    }

                    //le joueur joue sa carte
                    //Réception de l'action
                    Card card = getACardFromClient(socket, joueur);

                    //On la supprime
                    joueur.removeComponent(card);
                    //Si le joueur a joué une meilleure carte, il est déclaré vainqueur du round jusqu'a ce qu'un autre joueur mette une meilleure carte.
                    if (card.getValue() > maxValue) {
                        maxValue = card.getValue();
                        Vainqueur = joueur;
                        //Cette ligne permet d'empêche la bataille d'être à true si au final un joueur sort une plus grosse carte plus tard par exemple 7 - 7 - 10.
                        //Elle permet aussi la réinitialistion au début de chaque tour.
                        bataille = false;
                        //Si les valeurs sont égales on a (potentiellement) une bataille.
                    } else if (card.getValue().equals(maxValue)) {
                        bataille = true;
                    }
                    //On ajoute la carte et le joueur associé dans la liste.
                    playedCard.put(card, joueur);
                    board.addComponent(card);

                    //On affiche à chaque joueur la carte que vient de jouer le joueur jouant.
                    messageToAllClient(joueursConnecte, joueur.getName() + " has played " + card.getName());
                }
            }

            //Gestion de la bataille
            while (bataille) {

                //On affiche à chaque joueur le message de bataille
                messageToAllClient(joueursConnecte, "\nBattle !!!\n");

                //On extrait les joueurs à faire batailler
                for (Map.Entry<Card, Player> entry : playedCard.entrySet()) {
                    Card carte = entry.getKey();
                    int valeurCarte = carte.getValue();
                    Player joueur = entry.getValue();
                    if (valeurCarte == maxValue) {
                        //on évite de mettre plusieurs fois le même joueur. (peux être inutile)
                        if (!playerInBataille.containsKey(joueur)) {
                            Socket socket = joueursConnecte.get(joueur);
                            playerInBataille.put(joueur, socket);
                        }
                    }
                }
                //On extrait les joueurs qui ont joué des cartes ne leurs appartenant pas, si il y a eu une précédente bataille dans ce tour
                for (Map.Entry<Card, Player> entry : playedCardNotPossessed.entrySet()) {
                    Card carte = entry.getKey();
                    int valeurCarte = carte.getValue();
                    Player joueur = entry.getValue();
                    if (valeurCarte == maxValue) {
                        //on évite de mettre plusieurs fois le même joueur. (peux être inutile)
                        if (!playerInBataille.containsKey(joueur)) {
                            Socket socket = joueursConnecte.get(joueur);
                            playerInBataille.put(joueur, socket);
                        }
                    }
                }
                //On peux vider le HashMap des joueurs ayant joués des cartes ne leur appartenant pas.
                playedCardNotPossessed.clear();

                //on remet maxValue à 0 pour notre bataille
                maxValue = 0;
                //Ensuite on fait jouer les joueurs qui ont assez de cartes pour jouer.
                for (Map.Entry<Player, Socket> entry : playerInBataille.entrySet()) {
                    Player joueur = players.get(players.indexOf(entry.getKey()));
                    Socket socket = entry.getValue();

                    //Envois des données du jeu au joueur
                    messageToClient(socket, this);

                    //Si le joueur peux jouer la bataille, c'est tout bon
                    if (joueur.getScore() >= 2) {

                        //On indique au joueur qu'il doit jouer
                        messageToClient(socket, "You must play a hidden card !");

                        //On le fait jouer sa carte face caché
                        Card cardHidden = getACardFromClient(socket, joueur);

                        joueur.removeComponent(cardHidden);
                        cardHidden.setHidden(true);
                        playedCard.put(cardHidden, joueur);
                        board.addComponent(cardHidden);

                        //On affiche à chaque joueur le fait que le joueur a joué une carte face cachée
                        messageToAllClient(joueursConnecte, joueur.getName() + " play a hidden card ...");

                        //Envois des données du jeu au joueur pour éviter qu'il joue la même carte...
                        messageToClient(socket, this);

                        //puis on le fait jouer la 3ème carte
                        //On dit au client de jouer encore une fois...
                        messageToClient(socket, "[" + joueur.getName() + "] you have to play");


                        //récupération de la réponse
                        Card card = getACardFromClient(socket, joueur);


                        //On affiche à chaque joueur le fait que le joueur a joué sa carte
                        messageToAllClient(joueursConnecte, joueur.getName() + " has played " + card.getName() + "\n");

                        //Puis on lui enlève sa carte pour la mettre sur le plateau
                        joueur.removeComponent(card);
                        playedCard.put(card, joueur);
                        board.addComponent(card);
                        //Ensuite on réutilise notre système pour determiné si on a un gagnant ou une bataille.
                        if (card.getValue() > maxValue) {
                            maxValue = card.getValue();
                            Vainqueur = joueur;
                            bataille = false;
                        } else if (card.getValue().equals(maxValue)) {
                            bataille = true;
                        }
                        //Sinon, si notre joueur ne peux pas jouer la bataille jusqu'au bout, les choses se compliquent...
                    } else if (joueur.getScore() <= 1) {
                        playerInNeed.put(joueur, socket);
                    }
                    //Si à la fin le joueur a encore des cartes, ont dit qu'il peux aider.
                    if (joueur.getScore() > 0 && !playerInNeed.containsKey(joueur)) {
                        playerCanHelp.put(joueur, socket);
                    }
                }
                //On gère les joueurs n'ayant pas assez de carte
                for (Map.Entry<Player, Socket> entry : playerInNeed.entrySet()) {
                    Player joueur = players.get(players.indexOf(entry.getKey()));
                    Socket socket = entry.getValue();
                    //Si il lui reste une carte on l'ajoute au "pot"
                    if (joueur.getScore() == 1) {

                        //On indique au joueur qu'il doit jouer
                        messageToClient(socket, "You must play a hidden card !");

                        //On le fait jouer sa carte face caché
                        Card cardHidden = getACardFromClient(socket, joueur);

                        joueur.removeComponent(cardHidden);
                        cardHidden.setHidden(true);
                        playedCard.put(cardHidden, joueur);
                        board.addComponent(cardHidden);
                        //On affiche à chaque joueur le fait que le joueur a joué une carte face cachée
                        messageToAllClient(joueursConnecte, joueur.getName() + " play a hidden card ...");
                    }


                    //Si un des adversaires a encore une carte pour aider:
                    Player fournisseur = null;
                    Socket socketFournisseur = null;
                    if (!playerCanHelp.isEmpty()) {
                        Map.Entry<Player, Socket> fournisseurOfficiel = playerCanHelp.entrySet().iterator().next();
                        fournisseur = fournisseurOfficiel.getKey();
                        socketFournisseur = fournisseurOfficiel.getValue();
                    } else {
                        //Sinon on prend le joueur qui a le plus de carte
                        int maxCarte = 0;
                        for (Map.Entry<Player, Socket> mostScore : playerInNeed.entrySet()) {
                            Player playingPlayer = players.get(players.indexOf(mostScore.getKey()));
                            Socket socket1 = mostScore.getValue();
                            if (playingPlayer.getScore() >= maxCarte) {
                                fournisseur = playingPlayer;
                                socketFournisseur = socket1;
                            }
                        }
                    }

                    //On envois les données du jeu au fournisseur pour éviter qu'il ne donne la carte qu'il vient de jouer si c'est l'adversaire...
                    messageToClient(socketFournisseur, this);

                    //On indique au fournisseur qu'on lui emprunte une carte
                    //On indique au joueur qu'il doit jouer
                    messageToClient(socketFournisseur, "We borrow you a card so that [" + joueur.getName() + "] can play");

                    //récupération de la réponse
                    Card card = getACardFromClient(socketFournisseur, fournisseur);

                    //On affiche à tout les joueurs la carte que viens de jouer le joueur sans carte et de qui vient la dite carte
                    messageToAllClient(joueursConnecte, joueur.getName() + " has played " + card.getName() + " from " + fournisseur.getName() + "\n");

                    //On lie le joueur empruntant avec la carte jouer, si jamais il y a une nouvelle bataille après.
                    playedCardNotPossessed.put(card, joueur);
                    //Puis on compare le tout.
                    if (card.getValue() > maxValue) {
                        maxValue = card.getValue();
                        Vainqueur = joueur;
                        bataille = false;
                    } else if (card.getValue().equals(maxValue)) {
                        bataille = true;
                    }
                }
                //On oublie pas de nettoyer nos différents HashSet
                playerInBataille.clear();
                playerInNeed.clear();
                playerCanHelp.clear();
            }

            //Affichage plateau, puis du vainqueur et actualisation du jeu
            String separateur = "\n------------------------\n";
            messageToAllClient(joueursConnecte, this);
            messageToAllClient(joueursConnecte, board);
            messageToAllClient(joueursConnecte, separateur);
            messageToAllClient(joueursConnecte, Vainqueur.getName() + " win this round !\n");


            //le vainqueur raffle la mise, on vérifie aussi les joueurs qui ne peuvent plus jouer au passage
            for (Map.Entry<Card, Player> entry : playedCard.entrySet()) {
                Card carte = entry.getKey();
                Player ancienProprio = entry.getValue();
                if (carte.isHidden()) carte.setHidden(false);
                Vainqueur.addComponent(carte);
                if (ancienProprio.getScore() == 0 && ancienProprio.isPlaying()) {
                    ancienProprio.canPlay(false);
                    for (Map.Entry<Player, Socket> waiting : joueursConnecte.entrySet()) {
                        Player cible = waiting.getKey();
                        Socket socket = waiting.getValue();
                        if (ancienProprio != cible) {
                            messageToClient(socket, ancienProprio.getName() + " lost !!");
                        } else {
                            messageToClient(socket, "You have lost !");
                        }
                    }
                }
            }
            //on oublie pas de nettoyer les cartes jouées.
            playedCard.clear();
            board.clear();

            //affichage des scores
            StringBuilder score = new StringBuilder();
            for (Player joueur : players) {
                if (joueur.isPlaying()) {
                    score.append(joueur.getName()).append(" : ").append(joueur.getScore()).append("\n");
                }
            }
            messageToAllClient(joueursConnecte, "Round: " + numberOfRounds + " ; Score: \n"
                    + score.toString() +
                    "\n------------------------\n");


            //Si on atteint un modulo 10 round, tout les joueurs mélangent leurs mains.
            if (this.numberOfRounds % 10 == 0) {
                messageToAllClient(joueursConnecte, "Shuffle...");
                for (Player joueur : players) {
                    joueur.shuffleHand();
                }
            }

            //à la fin de chaque round on oublie pas d'incrémenter le compteur de rounds...
            numberOfRounds++;
        }

        return Vainqueur;
    }

    private void messageToClient(Socket socket, Object object) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject(object);
    }

    private void messageToAllClient(Map<Player, Socket> clientConnected, Object object) throws IOException {
        for (Map.Entry<Player, Socket> waiting : clientConnected.entrySet()) {
            Socket socket = waiting.getValue();
            messageToClient(socket, object);
        }
    }

    private Card getACardFromClient(Socket socket, Player joueur) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Card card = (Card) ois.readObject();
        for (Component carte : joueur.getSpecificComponents(Card.class)) {
            if (card.getValue().equals(carte.getValue())) {
                return (Card) carte;
            }
        }
        return null;
    }
}

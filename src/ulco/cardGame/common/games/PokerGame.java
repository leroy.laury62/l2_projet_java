package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.games.boards.PokerBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class PokerGame extends BoardGame {
    private List<Card> cards;
    private List<Coin> coins;
    private Integer numberOfRounds = 0;
    private Integer maxRounds = 3;

    public PokerGame(String name, Integer maxPlayers, String filename) {
        super(name, maxPlayers, filename);
        this.board = new PokerBoard();
    }

    public void initialize(String filename) {
        try {
            File cardFile = new File(filename);
            Scanner myReader = new Scanner(cardFile);
            this.cards = new ArrayList<>();
            this.coins = new ArrayList<>();
            while (myReader.hasNextLine()) {
                String[] data = myReader.nextLine().split(";");
                if (data[0].equals("Card")) this.cards.add(new Card(data[1], Integer.parseInt(data[2]), true));
                else if (data[0].equals("Chip")) this.coins.add(new Coin(data[1], Integer.parseInt(data[2])));
                else {
                    System.out.println("An error occurred !!! Data receved : " + data[0]);
                    break;
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred .");
            e.printStackTrace();
        }

    }


    public Player run(Map<Player, Socket> joueursConnecte) throws IOException, ClassNotFoundException {

        Object answer;

        //Variable joueur permettant de désigner le vainqueur de chaque round
        Player Vainqueur = null;

        //Liste pour stocker les valeurs des cartes
        List<Integer> valeurs = new ArrayList<>();

        //Variable pour stocker le jeton joué par le joueur
        Coin coinAnswer = null;

        //Compteur de joueur qui mise encore sur ce round.
        int joueurRestant = 0;

        //String pour séparer les blocs de données dans la console.
        String separateur = "\n------------------------\n";

        //On donne le même nombre de jetons à chaque joueur et on dit qu'ils sont prêt à jouer.
        for (Player joueur : players) {
            joueur.canPlay(true);
            joueurRestant++;
            for (Coin jeton : coins) {
                joueur.addComponent(jeton);
            }
        }

        //Affichage pour chaque joueur du nombre de joueurs et de leur score (pour dire que tout fonctionne bien)
        for (Map.Entry<Player, Socket> entry : joueursConnecte.entrySet()) {
            Socket socket = entry.getValue();
            StringBuilder statut = new StringBuilder();
            for (Player joueur : players) {
                if (joueur.isPlaying()) {
                    statut.append(joueur.getName()).append(" : ").append(joueur.getScore()).append("\n");
                }
            }
            statut.append(separateur);
            ObjectOutputStream toutEstOkOos = new ObjectOutputStream(socket.getOutputStream());
            toutEstOkOos.writeObject(statut);
        }

        //tout le temps que l'on n'a pas atteint le nombre max de round.
        while (!numberOfRounds.equals(maxRounds)) {
            //mélange des cartes
            Collections.shuffle(cards);

            //On distribue 3 cartes à chaque joueurs.
            for (int i = 0; i < 3; i++) {
                for (Player joueur : players) {
                    cards.get(0).setHidden(false);
                    joueur.addComponent(cards.remove(0));
                }
            }

            //on met les cartes restants sur la pile face cachée
            for (Card carte : cards) {
                carte.setHidden(true);
            }


            //chaque joueur mise:
            for (Map.Entry<Player, Socket> entry : joueursConnecte.entrySet()) {
                Player joueur = players.get(players.indexOf(entry.getKey()));
                Socket socket = entry.getValue();
                //On envoit l'état du jeu
                ObjectOutputStream gameOos = new ObjectOutputStream(socket.getOutputStream());
                gameOos.writeObject(this);
                //Et on fait signe au client qu'il doit regarder sa main...
                ObjectOutputStream checkYouHandOos = new ObjectOutputStream(socket.getOutputStream());
                checkYouHandOos.writeObject("Your hand :");


                //Si il reste au moins 2 joueurs pouvant miser, on fait miser tout les joueurs, sinon on passe au vainqueur
                if (joueurRestant > 1) {
                    //On envois un message à chaque joueur pour dire qui on attend
                    for (Map.Entry<Player, Socket> waiting : joueursConnecte.entrySet()) {
                        Player cible = waiting.getKey();
                        Socket socket1 = waiting.getValue();
                        String waitForAction = "";
                        if (cible != joueur) {
                            waitForAction += "Waiting for " + joueur.getName() + " to play...";
                        } else {
                            waitForAction += "Fold or bet ? If fold, please reply fold else reply the color of the coin\n" +
                                    "[" + cible.getName() + "] you have to play";
                        }
                        ObjectOutputStream waitForActionOos = new ObjectOutputStream(socket1.getOutputStream());
                        waitForActionOos.writeObject(waitForAction);
                    }

                    //Reception et traitement de la réponse
                    ObjectInputStream oisMise1 = new ObjectInputStream(socket.getInputStream());
                    answer = oisMise1.readObject();
                    //Si la réponse n'est pas un jeton ou alors fold on redemande une réponse
                    while (!answer.toString().equals("fold") && !(answer instanceof Coin)) {
                        String errorRetry = "Error in your answer... retry\n" +
                                "[" + joueur.getName() + "] you have to play";
                        ObjectOutputStream waitForActionErrorOos = new ObjectOutputStream(socket.getOutputStream());
                        waitForActionErrorOos.writeObject(errorRetry);
                        oisMise1 = new ObjectInputStream(socket.getInputStream());
                        answer = oisMise1.readObject();
                    }
                    //Si le joueur se couche on l'enlève des joueurs jouant ce tour
                    if (answer.toString().equals("fold")) {
                        joueur.canPlay(false);
                        joueurRestant--;
                        //Sinon on le fait jouer son jeton
                    } else if (answer instanceof Coin) {
                        coinAnswer = (Coin) answer;
                        for (Component jeton : joueur.getSpecificComponents(Coin.class)) {
                            if (jeton.getValue().equals(coinAnswer.getValue())) {
                                coinAnswer = (Coin) jeton;
                                break;
                            }
                        }
                        board.addComponent(coinAnswer);
                        joueur.removeComponent(coinAnswer);
                    }
                }
            }


            //on dévoile les 3 prochaines cartes sur le plateau
            for (int i = 0; i < 3; i++) {
                cards.get(0).setHidden(false);
                board.addComponent(cards.remove(0));
            }

            //On affichage l'état du plateau aux joueurs
            for (Map.Entry<Player, Socket> waiting : joueursConnecte.entrySet()) {
                Socket socket = waiting.getValue();
                ObjectOutputStream boardOos = new ObjectOutputStream(socket.getOutputStream());
                boardOos.writeObject(board);
            }

            //chaque joueur qui ne s'est pas couché mise à nouveau, si il ne reste plus assez de joueurs non coucher, on passe au vainqueur.
            for (Map.Entry<Player, Socket> entry : joueursConnecte.entrySet()) {
                Player joueur = players.get(players.indexOf(entry.getKey()));
                Socket socket = entry.getValue();

                //On envoit l'état du jeu
                ObjectOutputStream gameOos = new ObjectOutputStream(socket.getOutputStream());
                gameOos.writeObject(this);

                //Si il reste au moins 2 joueurs pouvant miser, on fait miser tout les joueurs, sinon on passe au vainqueur
                if (joueurRestant > 1 && joueur.isPlaying()) {
                    //On envois un message à chaque joueur pour dire qui on attend
                    for (Map.Entry<Player, Socket> waiting : joueursConnecte.entrySet()) {
                        Player cible = waiting.getKey();
                        Socket socket1 = waiting.getValue();
                        String waitForAction = "";
                        if (cible != joueur) {
                            waitForAction += "Waiting for " + joueur.getName() + " to play...";
                        } else {
                            waitForAction += "Fold or bet ? If fold, please reply fold else reply the color of the coin\n" +
                                    "[" + cible.getName() + "] you have to play";
                        }
                        ObjectOutputStream waitForActionOos = new ObjectOutputStream(socket1.getOutputStream());
                        waitForActionOos.writeObject(waitForAction);
                    }

                    //Reception et traitement de la réponse
                    ObjectInputStream oisMise2 = new ObjectInputStream(socket.getInputStream());
                    answer = oisMise2.readObject();
                    //Si la réponse n'est pas un jeton ou alors fold on redemande une réponse
                    while (!answer.toString().equals("fold") && !(answer instanceof Coin)) {
                        String errorRetry = "Error in your answer... retry\n" +
                                "[" + joueur.getName() + "] you have to play";
                        ObjectOutputStream waitForActionOosError = new ObjectOutputStream(socket.getOutputStream());
                        waitForActionOosError.writeObject(errorRetry);
                        oisMise2 = new ObjectInputStream(socket.getInputStream());
                        answer = oisMise2.readObject();
                    }
                    //Si le joueur se couche on l'enlève des joueurs jouant ce tour
                    if (answer.toString().equals("fold")) {
                        joueur.canPlay(false);
                        joueurRestant--;
                        //Sinon on le fait jouer son jeton
                    } else if (answer instanceof Coin) {
                        coinAnswer = (Coin) answer;
                        for (Component jeton : joueur.getSpecificComponents(Coin.class)) {
                            if (jeton.getValue().equals(coinAnswer.getValue())) {
                                coinAnswer = (Coin) jeton;
                                break;
                            }
                        }
                        board.addComponent(coinAnswer);
                        joueur.removeComponent(coinAnswer);
                    }
                }
            }

            //Valeurs du vainqueur.
            Integer sameOccurenceVainqueur = 1;
            Integer valueCardVainqueur = 0;
            //chaque joueur montre sa main, et on regarde si il est le vainqueur.
            for (Map.Entry<Player, Socket> entry : joueursConnecte.entrySet()) {
                Player joueur = players.get(players.indexOf(entry.getKey()));
                Socket socket = entry.getValue();
                if (joueur.isPlaying()) {
                    //Valeurs pour désigner l'affichage pour chaque joueur et comparé au vainqueur
                    Integer sameOccurence = 1;
                    Integer valueCard = 0;

                    //On réunis les valeurs des cartes de la main du joueur et de celles sur le plateau
                    List<Component> main = joueur.getSpecificComponents(Card.class);
                    for (Component carte : main) {
                        valeurs.add(carte.getValue());
                    }
                    for (Component carte : board.getSpecificComponents(Card.class)) {
                        valeurs.add(carte.getValue());
                    }
                    //pour chaque valeurs on détermine sa fréquence et sa valeur pour extraire la "main gagnante".
                    for (Integer value : valeurs) {
                        int frequency = Collections.frequency(valeurs, value);
                        //Si il y a plus d'occurence d'une même valeur, alors cette valeur est celle gagnante
                        if (frequency > sameOccurence) {
                            sameOccurence = frequency;
                            valueCard = value;
                            //Sinon si il y a le même nombre d'occurence mais que la valeur est supérieur, alors cette valeur est... supérieure.
                        } else if (frequency == sameOccurence && value > valueCard) {
                            valueCard = value;
                        }
                    }
                    //Comparaison au vainqueur
                    //Si il a un nombre d'occurence plus forte, il devient le vainqueur temporaire
                    if (sameOccurence > sameOccurenceVainqueur) {
                        Vainqueur = joueur;
                        sameOccurenceVainqueur = sameOccurence;
                        valueCardVainqueur = valueCard;
                    }
                    //Sinon si il a le même nombre d'occurence mais une valeur de carte plus forte, il devient le vainqueur.
                    else if (sameOccurence.equals(sameOccurenceVainqueur) && valueCard > valueCardVainqueur) {
                        Vainqueur = joueur;
                        valueCardVainqueur = valueCard;
                        //Si les valeurs et le nombre d'occurence sont identiques, le hasard décide du vainqueur.
                    } else if (sameOccurence.equals(sameOccurenceVainqueur) && valueCard.equals(valueCardVainqueur) && Math.random() > 0.5) {
                        Vainqueur = joueur;
                    }

                    //Affichage de ce que viens de jouer le joueur.
                    for (Map.Entry<Player, Socket> waiting : joueursConnecte.entrySet()) {
                        Socket socket1 = waiting.getValue();
                        Player cible = waiting.getKey();
                        if (joueur != cible) {
                            String justPlayed = joueur.getName() + " has " + sameOccurence + " same card(s) of value " + valueCard;
                            ObjectOutputStream justPlayedOos = new ObjectOutputStream(socket1.getOutputStream());
                            justPlayedOos.writeObject(justPlayed);
                        } else {
                            String justPlayed = "You have " + sameOccurence + " same card(s) of value " + valueCard;
                            ObjectOutputStream justPlayedOos = new ObjectOutputStream(socket1.getOutputStream());
                            justPlayedOos.writeObject(justPlayed);
                        }
                    }

                    //On oublie pas de nettoyer le tableau de valeur.
                    valeurs.clear();
                }
            }

            //Affichage du vainqueur à chaque joueur.
            for (Map.Entry<Player, Socket> entry : joueursConnecte.entrySet()) {
                Socket socket = entry.getValue();
                String vainqueurMessage = "\n-----\n" +
                        Vainqueur.getName() + " won the game with " + sameOccurenceVainqueur + " same card(s) of value " + valueCardVainqueur
                        + "\n-----\n";
                ObjectOutputStream vainqueurOos = new ObjectOutputStream(socket.getOutputStream());
                vainqueurOos.writeObject(vainqueurMessage);
            }


            //ajout des jetons au vainqueur
            for (Component jeton : board.getSpecificComponents(Coin.class)) {
                Vainqueur.addComponent(jeton);
            }

            //On peux vider le plateau en rajoutant les cartes dans le paquet
            for (Component carte : board.getSpecificComponents(Card.class)) {
                cards.add((Card) carte);
            }
            board.clear();

            //Si un joueur n'a plus de jetons, il quitte le jeu, on remet aussi les joueurs qui se sont couchés prêt à jouer le prochain round et on vide leurs mains au passage.
            for (Player joueur : players) {
                //on remet les cartes en mains dans le paquet
                for (Component carte : joueur.getSpecificComponents(Card.class)) {
                    cards.add((Card) carte);
                }
                joueur.clearHand();
                if (joueur.getScore() == 0) {
                    joueur.canPlay(false);
                    players.remove(joueur);
                    for (Map.Entry<Player, Socket> entry : joueursConnecte.entrySet()) {
                        Socket socket = entry.getValue();
                        ObjectOutputStream loseOos = new ObjectOutputStream(socket.getOutputStream());
                        loseOos.writeObject(joueur.getName() + " has no more coin and leave the game !");
                    }
                } else if (!joueur.isPlaying()) {
                    joueur.canPlay(true);
                    joueurRestant++;
                }
            }

            //Affichage des score des joueurs:
            for (Map.Entry<Player, Socket> entry : joueursConnecte.entrySet()) {
                Socket socket = entry.getValue();
                //Oos de séparateur ("--------")
                ObjectOutputStream separateurOos = new ObjectOutputStream(socket.getOutputStream());
                separateurOos.writeObject(separateur);
                for (Player joueur: players){
                    if (joueur.isPlaying()){
                        String message = "Score of " + joueur.getName() + " : " + joueur.getScore().toString() +"\n";
                        ObjectOutputStream score = new ObjectOutputStream(socket.getOutputStream());
                        score.writeObject(message);
                    }
                }
                ObjectOutputStream separateurOos2 = new ObjectOutputStream(socket.getOutputStream());
                separateurOos2.writeObject(separateur);
            }

            //On passe au round suivant.
            numberOfRounds++;
            for (Map.Entry<Player, Socket> entry : joueursConnecte.entrySet()) {
                Socket socket = entry.getValue();
                String message = "\n-----\n" +
                        "End of the round n°" + numberOfRounds + " of " + maxRounds
                        + "\n-----\n";
                ObjectOutputStream endOfRound = new ObjectOutputStream(socket.getOutputStream());
                endOfRound.writeObject(message);
            }

        }

        //on recherche le joueur qui a le plus haut score (si il y a égalité, il y a un tirage au sort).
        int score = 0;
        for (Player joueur : players) {
            if (joueur.getScore() > score) {
                Vainqueur = joueur;
                score = joueur.getScore();
            } else if (joueur.getScore().equals(score) && Math.random() > 0.5) {
                Vainqueur = joueur;
            }
        }

        return Vainqueur;
    }

    public boolean end() {
        return numberOfRounds.equals(maxRounds);
    }

    public String toString() {
        return this.name;
    }
}

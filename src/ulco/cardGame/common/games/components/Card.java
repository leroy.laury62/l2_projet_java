package ulco.cardGame.common.games.components;

public class Card extends Component{
    private boolean hidden;
    public Card(String name, Integer value, boolean hidden){
        super(name, value);
        this.hidden = hidden;
    }

    public boolean isHidden(){
        return hidden;
    }

    public void setHidden(boolean hidden){
        this.hidden = hidden;
    }

    public String toString() {
        return "Component{" +
                "name='" + name + '\'' +
                ", value=" + value +
                ", hidden= " + hidden +
                '}';
    }
}

package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public abstract class BoardGame implements Game {
    protected String name;
    protected Integer maxPlayers;
    protected List<Player> players = new ArrayList<>();
    protected boolean endGame;
    protected boolean started;
    protected Board board;

    public BoardGame(String name, Integer maxPlayers, String filename) {
        this.name = name;
        this.maxPlayers = maxPlayers;
        initialize(filename);
    }

    public boolean addPlayer(Socket socket, Player player) throws IOException {
        String message;
        if (players.size() == maxPlayers) {
            message="You can't connect ! Max numbers of players reached.";
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(message);
            ObjectOutputStream oos2 = new ObjectOutputStream(socket.getOutputStream());
            oos2.writeObject("END");
            return false;
        }
        for (Player joueur : players) {
            if (joueur.getName().equals(player.getName())) {
                message="You can't connect ! Your name already exist.";
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                oos.writeObject(message);
                ObjectOutputStream oos2 = new ObjectOutputStream(socket.getOutputStream());
                oos2.writeObject("END");
                return false;
            }
        }
        this.players.add(player);
        return true;
    }

    public void removePlayer(Player player) {
        if (!players.contains(player)) System.out.println(player.getName() + " didn't exist !");
        else players.remove(player);
    }

    public void removePlayers() {
        players.clear();
    }

    public void displayState(){
        System.out.println("------------------------------------------");
        System.out.println("--------------- Game State ---------------");
        System.out.println("------------------------------------------");
        for (Player joueur : players){
            System.out.println(joueur);
        }
    }

    public boolean isStarted(){
        this.started = this.players.size() == maxPlayers;
        return this.started;
    }

    public Integer maxNumberOfPlayers(){
        return this.maxPlayers;
    }

    public List<Player> getPlayers(){
        return this.players;
    }

    public Board getBoard(){return this.board;}
}

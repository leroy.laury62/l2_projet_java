package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PokerBoard implements Board {
    private List<Card> cards;
    private List<Coin> coins;

    public PokerBoard() {
        this.cards = new ArrayList<>();
        this.coins = new ArrayList<>();
    }

    public void clear() {
        this.cards.clear();
        this.coins.clear();
    }

    public void addComponent(Component component) {
        if (component.getClass() == Card.class) {
            this.cards.add((Card) component);
        } else if (component.getClass() == Coin.class) {
            this.coins.add((Coin) component);
        }
    }

    public List<Component> getComponents() {
        List<Component> components = new ArrayList<>(cards);
        components.addAll(coins);
        return components;
    }

    public List<Component> getSpecificComponents(Class classType) {
        if (classType == Card.class) return new ArrayList<>(cards);
        else if (classType == Coin.class) return new ArrayList<>(coins);
        return null;
    }

    public void displayState() {
        int totalJeton=0;
        System.out.println("\n---------- Board state ----------" );
        System.out.println("Cards : ");
        for (Card carte : cards) {
            if (!carte.isHidden()) {
                System.out.print(carte.getName() + " ");
            } else {
                System.out.print("Hidden ");
            }
        }
        System.out.println("\n-----");
        //triage des jetons pour un bel affichage :)
        HashMap<String, Integer> triage = new HashMap<>();
        for (Coin coin : coins){
            totalJeton+=coin.getValue();
            if (!triage.containsKey(coin.getName())){
                triage.put(coin.getName(), 1);
            } else {
                triage.replace(coin.getName(), triage.get(coin.getName())+1);
            }
        }
        for (Map.Entry<String, Integer> entry : triage.entrySet()) {
            String couleur = entry.getKey();
            Integer nombre = entry.getValue();
            System.out.println("- Coin " + couleur + " x " + nombre);
        }
        System.out.println("Value of the pot : " + totalJeton);
        System.out.println("---------------------");
    }
}

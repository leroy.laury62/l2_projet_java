package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.List;

public class CardBoard implements Board {
    private List<Card> cards;

    public CardBoard(){
        this.cards = new ArrayList<>();
    }

    public void clear() {
        this.cards.clear();
    }

    public void addComponent(Component component) {
        this.cards.add((Card) component);
    }

    public List<Component> getComponents() {
        return new ArrayList<>(cards);
    }

    public List<Component> getSpecificComponents(Class classType) {
        return new ArrayList<>(cards);
    }

    public void displayState() {
        System.out.println("Cartes sur le plateau :");
        for (Card carte : cards) {
            if (!carte.isHidden()) {
                System.out.print(carte.getName() + " ");
            } else {
                System.out.print("Hidden ");
            }
        }
        System.out.println();
    }
}

package ulco.cardGame.common.players;

import ulco.cardGame.common.interfaces.Player;

public abstract class BoardPlayer implements Player {
    protected String name;
    protected boolean playing;
    protected Integer score;

    public BoardPlayer(String name){
        this.name=name;
        this.score=0;
        this.playing=false;
    }

    public void canPlay(boolean playing){
        this.playing=playing;
    }

    public String getName(){
        return this.name;
    }

    public boolean isPlaying(){
        return this.playing;
    }

    public Integer getScore(){
        return this.score;
    }

    public String toString(){
        return String.format("\nName: %s, Score: %d, isPlaying: %b\n", getName(), getScore(), isPlaying());
    }
}

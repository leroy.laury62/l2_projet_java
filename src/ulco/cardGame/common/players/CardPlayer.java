package ulco.cardGame.common.players;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardPlayer extends BoardPlayer {
    List<Card> cards;

    public CardPlayer(String name) {
        super(name);
        cards = new ArrayList<>();
    }

    public Integer getScore(){
        return this.score;
    }

    public void addComponent(Component component){
        cards.add((Card) component);
        this.score++;
    }

    public void removeComponent(Component component){
        if (cards.contains((Card) component)){
            cards.remove((Card) component);
            this.score--;
        } else {
            System.out.println("Card not found !\n");
        }
    }

    public void play(Socket socket) throws IOException {
        Card carte = cards.get(0);
        ObjectOutputStream cardOos = new ObjectOutputStream(socket.getOutputStream());
        cardOos.writeObject(carte);
    }

    public List<Component> getComponents(){
        return new ArrayList<>(cards);
    }

    @Override
    public List<Component> getSpecificComponents(Class classType) {
        return new ArrayList<>(cards);
    }

    public void shuffleHand(){
        Collections.shuffle(cards);
    }

    public void clearHand(){
        cards.clear();
    }

    @Override
    public void displayHand() {
        System.out.println("Hand of " + this.name + " :");
        for (Card cartes : cards){
            System.out.print(cartes.getName() + " ");
        }
        System.out.println("\n");
    }

    public String toString(){
        return String.format("\nName: %s, Score: %d, isPlaying: %b", getName(), getScore(), isPlaying());
    }
}

package ulco.cardGame.common.players;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.*;

public class PokerPlayer extends BoardPlayer {
    List<Card> cards;
    List<Coin> coins;

    public PokerPlayer(String name) {
        super(name);
        cards = new ArrayList<>();
        coins = new ArrayList<>();
    }

    public Integer getScore() {
        Integer score = 0;
        for (Coin jeton : coins) {
            score += jeton.getValue();
        }
        return score;
    }

    public void play(Socket socket) throws IOException {
        Scanner scanner = new Scanner(System.in);
        boolean foundOrFold = false;
        //Tant que la valeur n'est identique à l'un des jetons du joueur ou alors à fold.
        while (!foundOrFold) {
            System.out.println(this.name + ", please select a valid Coin to play (coin color) or fold ");
            //On demande une saisie.
            String couleurJeton = scanner.next();
            //On compare si on trouve un jeton de la bonne valeur ou si le joueur veut se coucher
            if (couleurJeton.equals("fold")) {
                ObjectOutputStream foldOos = new ObjectOutputStream(socket.getOutputStream());
                foldOos.writeObject(couleurJeton);
                foundOrFold = true;
            } else {
                for (Coin jeton : coins) {
                    if (couleurJeton.equals(jeton.getName())) {
                        ObjectOutputStream jetonOos = new ObjectOutputStream(socket.getOutputStream());
                        jetonOos.writeObject(jeton);
                        foundOrFold = true;
                        break;
                    }
                }
            }
            if (!foundOrFold) System.out.println("Invalid value !");
        }
    }

    public void addComponent(Component component) {
        if (component instanceof Card) {
            cards.add((Card) component);
        } else if (component instanceof Coin) {
            coins.add((Coin) component);
            score += component.getValue();
        }
    }

    public void removeComponent(Component component) {
        //Si on enlève une carte:
        if (component instanceof Card) {
            if (cards.contains(component)) {
                cards.remove(component);
            } else {
                System.out.println("Card not found !\n");
            }
            //Si on enlève un jeton
        } else if (component instanceof Coin) {
            if (coins.contains(component)) {
                coins.remove(component);
                score -= component.getValue();
            } else {
                System.out.println("Coin not found !\n");
            }
            //Si on enlève quelque chose d'autre
        } else {
            System.out.println("Can't remove component, instance error !");
        }
    }

    public List<Component> getComponents() {
        List<Component> components = new ArrayList<>(cards);
        components.addAll(coins);
        return components;
    }

    public List<Component> getSpecificComponents(Class classType) {
        if (classType == Card.class) return new ArrayList<>(cards);
        else if (classType == Coin.class) return new ArrayList<>(coins);
        return null;
    }

    public void shuffleHand() {
        Collections.shuffle(cards);
    }

    public void clearHand() {
        cards.clear();
    }

    public void displayHand() {
        System.out.println("\n---------------------");
        System.out.print("Hand of " + this.name);
        System.out.print("\n-----");
        System.out.println("\nCards : ");
        for (Card cartes : cards) {
            System.out.print(cartes.getName() + " ");
        }
        System.out.println("\n-----");
        System.out.println("Coins : ");
        //triage des jetons pour un bel affichage :)
        HashMap<String, Integer> triage = new HashMap<>();
        for (Coin coin : coins) {
            if (!triage.containsKey(coin.getName())) {
                triage.put(coin.getName(), 1);
            } else {
                triage.replace(coin.getName(), triage.get(coin.getName()) + 1);
            }
        }
        for (Map.Entry<String, Integer> entry : triage.entrySet()) {
            String couleur = entry.getKey();
            Integer nombre = entry.getValue();
            System.out.println("- Coin " + couleur + " x " + nombre);
        }
        System.out.println("\nCoins sum of " + this.name + " : " + getScore());
        System.out.println("---------------------");
    }

    public String toString() {
        return String.format("\nName: %s, Score: %d, isPlaying: %b", getName(), getScore(), isPlaying());
    }
}

package ulco.cardGame.client;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.games.boards.PokerBoard;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.server.SocketServer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

public class Client {


    public static void main(String[] args) {

        Game game = null;
        Player mySelf = null;
        // Current use full variables
        try {

            // Create a connection to the server socket on the server application
            InetAddress host = InetAddress.getLocalHost();
            Socket socket;

            Object answer;
            String username;

            Scanner scanner = new Scanner(System.in);

            System.out.println("Please select your username");
            username = scanner.nextLine();

            socket = new Socket(host.getHostName(), SocketServer.PORT);

            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(username);

            do {
                // Read and display the response message sent by server application
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                answer = ois.readObject();

                // depending of object type, we can manage its data
                if (answer instanceof String | answer instanceof StringBuilder) {
                    System.out.println(answer.toString());
                    //Si le joueur doit jouer, il joue
                    if (answer.toString().endsWith("[" + username + "] you have to play") || answer.toString().equals("You must play a hidden card !") ||
                    answer.toString().startsWith("We borrow you a card so that")){
                        List<Player> joueurs = game.getPlayers();
                        for (Player joueur : joueurs) {
                            if (joueur.getName().equals(username)) {
                                mySelf = joueur;
                            }
                        }
                        mySelf.play(socket);
                    }
                    //Si c'est la fin d'un round on affiche l'état du jeu
                    if (answer.toString().endsWith(" win this round !\n") || answer.toString().equals(game.toString())) {
                        game.displayState();
                        System.out.println("\n------------\n");
                    }
                    //Si on lui affiche sa main, il la regarde...
                    if (answer.toString().equals("Your hand :")) {
                        List<Player> joueurs = game.getPlayers();
                        for (Player joueur : joueurs) {
                            if (joueur.getName().equals(username)) {
                                mySelf = joueur;
                            }
                        }
                        mySelf.displayHand();
                    }
                }
                if (answer instanceof Game) {
                    game = ((Game) answer);
                }
                if (answer instanceof PokerBoard) {
                    System.out.println("\n------------------------\n");
                    ((PokerBoard) answer).displayState();
                }
                if (answer instanceof CardBoard) {
                    System.out.println("\n------------------------\n");
                    ((CardBoard) answer).displayState();
                }
            } while (!answer.equals("END"));

            // close the socket instance connection
            socket.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

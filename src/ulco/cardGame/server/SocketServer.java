package ulco.cardGame.server;

import ulco.cardGame.common.games.BattleGame;
import ulco.cardGame.common.games.CardGame;
import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.CardPlayer;
import ulco.cardGame.common.players.PokerPlayer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Server Game management with use of Singleton instance creation
 */
public class SocketServer {

    private ServerSocket server;
    protected Game game; // game will be instantiate later
    protected Map<Player, Socket> playerSockets;
    protected Constructor playerConstructor;

    public static final int PORT = 7777;

    private SocketServer() {
        try {
            server = new ServerSocket(PORT);
            playerSockets = new HashMap<>();
            playerConstructor = null;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SocketServer server = new SocketServer();
        server.run();
    }

    private void run() {

        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Choose your game : (Battle/Poker)");
            String jeu = scanner.next();
            while (!jeu.equals("Battle") && !jeu.equals("Poker")){
                System.out.println("Error ! \nChoose your game : (Battle/Poker)");
                jeu = scanner.next();
            }
            if (jeu.equals("Poker")) {
                // Need to specify your current cardGame.txt file
                game = new PokerGame("Poker", 3, "resources/games/pokerGame.txt");
            } else {
                game = new BattleGame("Battle", 3, "resources/games/cardGame.txt");
            }

            // Game Loop
            System.out.println("Waiting for new player for " + game.toString());

            // add each player until game is ready (or number of max expected player is reached)
            // Waiting for the socket entrance
            do {

                Socket socket = server.accept();

                // read socket Data
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                String playerUsername = (String) ois.readObject();

                // Create player instance
                Player player;
                if (jeu.equals("Poker")) {
                    player = new PokerPlayer(playerUsername);
                } else {
                    player = new CardPlayer(playerUsername);
                }

                // Si on peux ajouter le joueur:
                if (game.addPlayer(socket, player)) {

                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(game);
                    ObjectOutputStream oos2 = new ObjectOutputStream(socket.getOutputStream());
                    oos2.writeObject(game.toString());

                    // Tell to other players that new player is connected
                    for (Socket playerSocket : playerSockets.values()) {

                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                        playerOos.writeObject("Now connected: " + player.getName());
                    }

                    // Store player's socket in dictionary (Map)
                    playerSockets.put(player, socket);

                    System.out.println(player.getName() + " is now connected...");
                }


            } while (!game.isStarted());

            // run the whole game using sockets
            // TODO : Player gameWinner = game.run(playerSockets);
            Player gameWinner = game.run(playerSockets);
            // Tells to player that server will be closed (just as example)
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream winnerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                winnerOos.writeObject("---------------------\n" +
                        gameWinner.getName() + " win this game !!!\n" +
                        "---------------------\n");
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("END");
            }

            // Close each socket when game is finished
            for (Socket socket : playerSockets.values()) {
                socket.close();
            }

            game.removePlayers();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}


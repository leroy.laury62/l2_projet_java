package ulco.cardGame.server;

import ulco.cardGame.common.games.BattleGame;
import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.players.CardPlayer;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.PokerPlayer;

/**
 * Server Game management with use of Singleton instance creation
 */
public class GameServer {

    public static void main(String[] args) {

        System.out.println("Here the created games will be launched!");
        Game game = new PokerGame("Battle", 4, "resources/games/pokerGame.txt");

        Player player1 = new PokerPlayer("Bob");
        Player player2 = new PokerPlayer("Tom");
        /*Player player3 = new PokerPlayer("Robert");
        Player player4 = new PokerPlayer("Jean");

        game.addPlayer(player1);
        game.addPlayer(player2);
        game.addPlayer(player3);
        game.addPlayer(player4);*/

        game.displayState();
        //Player Vainqueur = game.run();

        //System.out.println("\n--- " + Vainqueur.getName() + " WIN !!! ---");
    }
}

